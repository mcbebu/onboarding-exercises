from fastapi import FastAPI
import uvicorn

app = FastAPI(
    title="Team X Onboarding",
    version="0.0.1",
    description="Hello world API",
)

@app.get("/")
async def read_root():
    return {
        "message": "hello world"
    }